'use strict';

// Declare app level module which depends on views, and components
angular.module("myFirstApp", ['ngRoute'])
.config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider){
		$routeProvider.when('/add-event', {
			templateUrl:'views/add-event.html',
			controller: 'formCtrl',
			controllerAs: 'eventCtl'
		})
		.when('/list-event/', {
			templateUrl:'views/list-event.html'
		})
		.otherwise({templateUrl:'views/404.html'})
		$locationProvider.html5Mode(true);
}])

