'use strict';
 
angular.module('myApp.home', ['ngRoute', 'firebase'])
 
// Declared route 
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'home/home.html',
        controller: 'HomeCtrl'
    });
}])
 
// Home controller
.controller('HomeCtrl', ["$scope", "$firebaseAuth",
  function($scope, $firebaseAuth) {
	$scope.SignIn = function(event){
		event.preventDefault();
		var username = $scope.user.email
		var password = $scope.user.password
		firebase.auth().signInWithEmailAndPassword(username, password).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  alert(error);
  // ...
});
		firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    alert("user signed in");
  } else {
    alert("authentication failed");
  }
});
            
  }
 
}]);