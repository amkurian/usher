angular.module("myFirstApp")
.controller('formCtrl',['eventFactory', function(eventFactory){
   this.event = eventFactory.getAllEvents();
   this.categories = [{id:1, name:'music'},{id:2, name:'cinema'},{id:3, name:'literature'}];
   this.submitForm = function(form){
   	eventFactory.createEvent(angular.copy(form), this.event);
   }
}])